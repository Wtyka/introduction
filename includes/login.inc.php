<?php

if (isset($_POST["submit"])) {

  $name = $_POST["email"];
  $pwd = $_POST["password"];

  require_once 'dbh.inc.php';
  require_once 'functions.inc.php';

  if (emptyInputLogin($name, $pwd) !== false) {
    header('location: ../login.php?error=emptyinput');
    exit();
  }
  if (invalidUid() !== false) {
    header('location: ../login.php?error=emptyinput');
    exit();
  }
} else {
  header('location: ../login.php');
  exit();
}
