window.addEventListener( 'load', () =>
{
  loginPopup( document.querySelector( '.btn__login' ), document.querySelector( '.login__popup' ) )
} )

function loginPopup ( btn, popup )
{
  btn.addEventListener( 'click', () =>
  {
    if ( !btn.active )
    {
      btn.active = true;
      setProperty( popup, { display: 'flex' } )
    }
    else
    {
      btn.active = false;
      setProperty( popup, { display: 'none' } )
    }
  } );
}

function setProperty ( element, styles )
{
  for ( const property in styles )
  {
    element.style.setProperty( property, styles[ property ] )
  }
}