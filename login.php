<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Aplikacja testowa">
  <meta name="author" content="Wojciech Wojtyczka">
  <title>Login</title>
  <link rel="stylesheet" href="./css.min/login.min.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <!-- <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
      rel="stylesheet"> -->
  <script src="./scripts/login.js" defer></script>

</head>

<body>
  <?php
  include_once 'header.php';
  ?>
  <div class="login__popup">
    <div class="login__form">
      <form action="./includes/login.inc.php" method="POST">
        <h3>Log in</h3>
        <label for="username">
          <input type="email" name="email" id="username" placeholder="Username">
        </label>
        <label for="password">
          <input type="password" name="password" id="password" placeholder="Password">
        </label>
        <button class="login__submit btn">Log in</button>
      </form>
    </div>
  </div>
  <?php
  include_once 'footer.php';
  ?>
</body>

</html>